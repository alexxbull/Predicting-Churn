import numpy as np
import pandas as pd

#-----------DATA PREPROCESSING-----------
# Importing the dataset
dataset = pd.read_csv('Churn_Modeling.csv')
x = dataset.iloc[:, 3:13].values   # independent variables
y = dataset.iloc[:, 13].values   # dependent variables

# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_x_1 = LabelEncoder()
x[:, 1] = labelencoder_x_1.fit_transform(x[:, 1])
labelencoder_x_2 = LabelEncoder()
x[:, 2] = labelencoder_x_2.fit_transform(x[:, 2])
onehotencoder = OneHotEncoder(categorical_features = [1])
x = onehotencoder.fit_transform(x).toarray()
x = x[:, 1:]

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

# Normalize the data
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
x_train = sc.fit_transform(x_train)
x_test = sc.transform(x_test)

#---------------BUILD ANN------------
import keras
from keras.models import Sequential
from keras.layers import Dense

# Initialize ANN
ann = Sequential()

# Add the input layer and the first hidden layer
ann.add(Dense(units=6, input_dim=11, kernel_initializer="uniform", activation="relu"))

# Add the second hidden layer
ann.add(Dense(units=6, kernel_initializer="uniform", activation="relu"))

# Add the output layer
ann.add(Dense(units=1, kernel_initializer="uniform", activation="sigmoid"))

#Compile the ANN
ann.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])

# Fitting the ANN to the Training Set
print("Training ANN on Training Set")
ann.fit(x_train, y_train, batch_size=10, epochs=100)

#--------------MAKING PREDICATIONS AND EVALUATING THE MODEL-------------
# Predicting the Test set results
print("\nTesting ANN on Test Set\n")
y_pred = ann.predict(x_test)
y_pred = (y_pred > 0.5)     # if probablity is less than 50% then predict customer will leave

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

# Print results
print("Confusion Matrix:\n", cm)
print("Accuracy Rate: %0.2f%%" % ( ((cm[0][0] + cm[1][1]) / len(y_test) ) * 100 ))

#--------------INTERPRETING RESULTS-------------

#weights = ann.model.get_weights()