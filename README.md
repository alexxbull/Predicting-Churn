# Predicting Churn
Predict whether a customer will leave or stay given a sufficient dataset.

## Credits
Dataset provided by SuperDataScience - https://www.superdatascience.com/deep-learning/
